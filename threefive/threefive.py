"""The module contains ThreeFive program implementation."""


def threefive():
    """Get resulting "ThreeFive" string.
    Generate string of numbers from 1 to 100,
    where multiples of 3 replaced with word "Three",
    multiples of 5 replaced with "Five" and multiples
    of both 3 and 5 replaced with "ThreeFive"

    :return: resulting numbers string.
    :rtype: str.
    """
    items_list = list(_iter_threefive())
    return ' '.join(items_list)


def _iter_threefive():
    """ThreeFive generator.

    Comment: A one-line implementation avoided
    in order to improve the readability.
    """
    for i in range(1, 101):
        item = ''
        if not i % 3:
            item = 'Three'
        if not i % 5:
            item += 'Five'
        if not item:
            item = str(i)
        yield item


if __name__ == '__main__':
    print(threefive())
