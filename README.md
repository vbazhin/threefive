### 1. Implementation

Language: Python 3.6.

### 2. Run app

```
$ cd threefive
$ python3 threefive/threefive.py
```

### 3. Run unit tests

```
$ cd threefive
$ python -m unittest tests/unit/test_threefive.py

# .
# ----------------------------------------------------------------------
# Ran 1 test in 0.000s

```


